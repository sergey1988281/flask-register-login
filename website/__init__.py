import logging
from os import path, makedirs
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy_utils import database_exists, create_database
from flask_migrate import Migrate
from sqlalchemy_utils import database_exists, create_database

# Configure FLASK and SQLAlchemy
app = Flask(__name__)
app.config.from_pyfile("settings.py")
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# Debugging
if app.config["DEBUG_TB_PROFILER_ENABLED"]:
    from flask_debugtoolbar import DebugToolbarExtension
    app.debug = True
    toolbar = DebugToolbarExtension(app)

# Configure logging
logger = logging.getLogger(__name__)
logger.setLevel(app.config["LOGGING_LEVEL"])
formatter = logging.Formatter(app.config["LOGGING_FORMAT"])
file_handler = logging.FileHandler(app.config["LOGGING_TARGET_FILE"])
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


# Import routes
import website.routes

# Check if database is ok and create if not
if not database_exists(app.config['SQLALCHEMY_DATABASE_URI']):
    logger.info(f"Initializing DB at {app.config['SQLALCHEMY_DATABASE_URI']}")
    try:
        create_database(app.config['SQLALCHEMY_DATABASE_URI'])
    except Exception as err:
        logger.critical(str(err))
        raise err
try:
    db.create_all()
except Exception as err:
    print(err)

# Create nessesary directories if not exist
if not path.exists(app.config["STATIC_FOLDER"] + app.config["PICTURE_FOLDER"]):
    makedirs(app.config["STATIC_FOLDER"] + app.config["PICTURE_FOLDER"])
    logger.info(f"{app.config['PICTURE_FOLDER']} folder created")

if not path.exists(app.config["STATIC_FOLDER"] + app.config["UPLOAD_FOLDER"]):
    makedirs(app.config["STATIC_FOLDER"] + app.config["UPLOAD_FOLDER"])
    logger.info(f"{app.config['UPLOAD_FOLDER']} folder created")

logger.info("Starting...")

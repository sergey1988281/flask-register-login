from flask_wtf import FlaskForm
from wtforms import (StringField, PasswordField, SubmitField, EmailField,
                     DateField)
from wtforms.validators import DataRequired, Regexp, Length, EqualTo, Email
from website import app


class RegisterForm(FlaskForm):
    username = StringField(
                    "Username: ",
                    validators=[
                        DataRequired("Cannot be empty"),
                        Regexp(app.config["USERNAME_POLICY"]["restriction"],
                               message=app.config["USERNAME_POLICY"]
                                                 ["restriction_notice"])])
    password1 = PasswordField(
                    "Password: ",
                    validators=[
                        DataRequired("Cannot be empty"),
                        Length(min=app.config["PASSWORD_POLICY"]["min_len"],
                               message=app.config["PASSWORD_POLICY"]
                                                 ["min_len_notice"]),
                        Regexp(app.config["PASSWORD_POLICY"]["restriction"],
                               message=app.config["PASSWORD_POLICY"]
                                                 ["restriction_notice"])])
    password2 = PasswordField(
                    "Repeat password: ",
                    validators=[EqualTo("password1",
                                        message="Passwords are not equal")])
    name = StringField("Name: ",
                       validators=[DataRequired("Cannot be empty"),
                                   Regexp(r"^[A-Za-z][A-Za-z ]*[A-Za-z]$",
                                          message=("Username must start with "
                                                   "letter, contains only "
                                                   "letters and spaces, end "
                                                   "with letter"))])
    email = EmailField("Email: ",
                       validators=[DataRequired("Cannot be empty"),
                                   Email("Incorrect email")])
    birthdate = DateField("Birthday: ",
                          validators=[DataRequired("Cannot be empty")])
    submit = SubmitField("Sign Up")

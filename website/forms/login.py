from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Regexp, Length
from website import app


class LoginForm(FlaskForm):
    username = StringField(
                    "Username: ",
                    validators=[
                        DataRequired("Cannot be empty"),
                        Regexp(app.config["USERNAME_POLICY"]["restriction"],
                               message=app.config["USERNAME_POLICY"]
                                                 ["restriction_notice"])])
    password = PasswordField(
                   "Password: ",
                   validators=[
                       DataRequired("Cannot be empty"),
                       Length(min=app.config["PASSWORD_POLICY"]["min_len"],
                              message=app.config["PASSWORD_POLICY"]
                                                ["min_len_notice"]),
                       Regexp(app.config["PASSWORD_POLICY"]["restriction"],
                              message=app.config["PASSWORD_POLICY"]
                                                ["restriction_notice"])])
    submit = SubmitField("Sign In")

from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileRequired
from wtforms import SubmitField, FileField
from website import app

allowed_ext = r",".join(app.config["ALLOWED_PICTURE_EXTENSIONS"])


class ChangePictureForm(FlaskForm):
    picture = FileField(
        "Change your picture:",
        validators=[FileRequired(message="You must provide a file"),
                    FileAllowed(app.config["ALLOWED_PICTURE_EXTENSIONS"],
                                message=f"Allowed extensions: {allowed_ext}")])
    submit = SubmitField("Change")


class RotatePictureForm(FlaskForm):
    submit = SubmitField("Rotate")


class SavePictureForm(FlaskForm):
    submit = SubmitField("Save")

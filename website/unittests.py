import unittest
import re
import bcrypt


def check_user_valid_data(user_data: dict) -> list:
    """
    This function checks if valid data was entered using different
    regular expressions which is used in WTForms RegExp validators
    """
    res = []
    # Check if username is valid
    if not re.match(r"^[A-Za-z][A-Za-z0-9-_]*[A-Za-z0-9]$",
                    user_data["username"]):
        res.append("Username must start with letter, contains only "
                   "letters, digits, dashes or underscores")
    # Check if password 1 contains valid characters
    # and has appropriate length
    if len(user_data["password1"]) < 8:
        res.append("Password must contain minimum 8 characters")
    if not re.match(r"^[A-Za-z0-9-_\$%]*$", user_data["password1"]):
        res.append("Password must not contain special characters "
                   "except % $ - _")
    # Check if password 1 match password 2
    if user_data["password1"] != user_data["password2"]:
        res.append("Passwords are not equal")
    # Check if name is valid
    if not re.match(r"^[A-Za-z][A-Za-z ]*[A-Za-z]$", user_data["name"]):
        res.append("Username must start with letter, "
                   "contains only letters and spaces, end with letter")
    # Email check
    if not re.match(
            r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$',
            user_data["email"]):
        res.append("Email is invalid")
    return res


def password_checks(input: str) -> bool:
    """Encoding and decoding of passwords example"""
    password = input
    password_bytes = password.encode("utf-8")
    password_hash = bcrypt.hashpw(password_bytes, bcrypt.gensalt())
    password_hash_str = password_hash.decode("utf-8")
    return bcrypt.checkpw(password.encode("utf-8"),
                          password_hash_str.encode("utf-8"))


class ValidateUser(unittest.TestCase):
    """User Class methods unit tests"""
    def test_incorrect_login(self):
        """Incorrect input test all possible scenarious"""
        input = {
            "username": "1ab",
            "password1": "?^~",
            "password2": "some",
            "name": "Ser1gey",
            "email": "123@123"
            }
        expected_output = [
            ("Username must start with letter, contains only letters, digits,"
             " dashes or underscores"),
            "Password must contain minimum 8 characters",
            "Password must not contain special characters except % $ - _",
            "Passwords are not equal",
            ("Username must start with letter, contains only letters and "
             "spaces, end with letter"),
            "Email is invalid"
            ]
        output = check_user_valid_data(input)
        self.assertEqual(expected_output, output)

    def test_correct_login(self):
        """Valid input test"""
        input = {
            "username": "Joe_doe",
            "password1": "Test1234",
            "password2": "Test1234",
            "name": "Joe Doe",
            "email": "john.doe@example.com"
            }
        expected_output = []
        output = check_user_valid_data(input)
        self.assertEqual(expected_output, output)

    def test_password_check(self):
        """Test cryptography algorythm"""
        self.assertTrue(password_checks("some_very_strong_password"))

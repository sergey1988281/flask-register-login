from website import db, app
from os import path
from .picture_service import PictureService
from ..models import User
from ..models import Profile
from werkzeug.utils import secure_filename
from werkzeug.datastructures import FileStorage


class ProfileService:
    @staticmethod
    def upload_picture(username: str):
        """Method uploads picture from server to database
        Args:
            username (str): Name of user to upload picture for
        Raises:
            err: SQL Alchemy error
        """
        picture_path = ProfileService.get_picture_filepath(username)
        userid = db.session.query(User.id).filter_by(login=username).subquery()
        profile = Profile.query \
            .filter_by(user_id=userid) \
            .first()
        profile.picture = open(picture_path, 'rb').read()
        try:
            db.session.commit()
        except Exception as err:
            db.session.rollback()
            raise err

    @staticmethod
    def get_picture_filepath(username: str) -> str:
        """Returns picture filepath for specific user
        Args:
            username (str): Login of user to get picture path
        Returns:
            str: Picture filepath
        """
        return path.join(app.config["STATIC_FOLDER"] +
                         app.config["PICTURE_FOLDER"],
                         username + ".png")

    @staticmethod
    def ensure_picture_locally(username: str, user_id: int) -> str | None:
        """Check if picture for profile is already on the server to
        minimize DB queries, otherwise download it from DB
        Args:
            username (str): Login of user to get picture
            user_id (int): Id of user to get picture if needed
        Returns:
            str | None: If picture exist locally or on DB return path
            relative from static folder otherwise return None
        """
        picture = app.config["PICTURE_FOLDER"] + username + ".png"
        if not path.exists(app.config["STATIC_FOLDER"] + picture):
            picture_bin = Profile.query \
                .filter_by(user_id=user_id) \
                .with_entities(Profile.picture) \
                .first()
            if not picture_bin["picture"]:
                return None
            else:
                with open(app.config["STATIC_FOLDER"] + picture, 'wb') as f:
                    f.write(picture_bin["picture"])
        return picture

    @staticmethod
    def save_picture_locally(username: str,
                             file: FileStorage) -> str:
        """Saves picture to local storage in temporary and final state
        Args:
            username (str): Login of user
            file (FileStorage): File object
        Returns:
            str: Final filepath after resize and change format
        """
        filename = file.filename
        if filename == "":
            return ""
        filename = secure_filename(filename)
        temp_filepath = path.join(app.config["STATIC_FOLDER"] +
                                  app.config["UPLOAD_FOLDER"],
                                  filename)
        file.save(temp_filepath)
        final_filepath = ProfileService.get_picture_filepath(username)
        PictureService.resize_convert_picture(
            temp_filepath,
            final_filepath,
            app.config["MAX_PICTURE_SIZE"])
        return final_filepath

from PIL import Image


class PictureService:
    """Utility class holds methods to manipulate images
    """
    @staticmethod
    def resize_convert_picture(oldfilename: str,
                               newfilename: str,
                               size: tuple[int, int]):
        """Resizes and converts all uploaded pictures to png
        Args:
            oldfilename (str): Location of old file
            newfilename (str): Location of new file to save
            size (tuple[int, int]): Size to squeeze
        """
        with Image.open(oldfilename) as image:
            image.thumbnail(size)
            image.save(newfilename)

    @staticmethod
    def rotate_picture_90(picture_path: str):
        """Rotates picture 90 degrees
        Args:
            picture_path (str): Location of picture to rotate
        """
        angle = 90
        with Image.open(picture_path) as image:
            out = image.rotate(angle, expand=True)
            out.save(picture_path)

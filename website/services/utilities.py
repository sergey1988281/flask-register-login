from flask import Request


def log_msg(request: Request, message: str) -> str:
    """This function returns pretty format for log messages
    Args:
        request (Request): Flask Request object
        message (str): Message to wrap
    Returns:
        (str): Wrapped log message
    """
    return (f" {str(request.remote_addr)} {str(request.user_agent)}"
            f"{str(request.method)} {message}")

import bcrypt
from sqlalchemy import exc
from datetime import date
from website import db, logger
from ..models.user import User
from ..models.profile import Profile


class UserService():
    @staticmethod
    def register_user(username: str,
                      password: str,
                      email: str,
                      birth_date: date,
                      ):
        """Performs check if user exist in a database
        and saves information if all is ok
        Args:
            username (str): User login
            password (str): User password
            email (str): User email
            birth_date (date): User birthday

        Raises:
            ValueError: ValueError in case login or email already exist
            ValueError: Generic exception if somethinf is wrong
        """
        user = User(
            login=username,
            password_hash=bcrypt.hashpw(
                                        password.encode('utf-8'),
                                        bcrypt.gensalt()).decode("utf-8"))
        db.session.add(user)
        try:
            db.session.flush()
        except (exc.IntegrityError, Exception) as err:
            db.session.rollback()
            if isinstance(err, exc.IntegrityError):
                logger.error(
                        (f"Registration: User with username: "
                         f"{username} already exist"))
                raise ValueError("User with such username already exist")
            else:
                logger.error(f"Registration: " + str(err))
                raise
        profile = Profile(name=username,
                          email=email,
                          birth_date=birth_date,
                          user_id=user.id)
        db.session.add(profile)
        try:
            db.session.commit()
        except (exc.IntegrityError, Exception) as err:
            db.session.rollback()
            if isinstance(err, exc.IntegrityError):
                logger.error((f"Registration: User with email: "
                              f"{email} already exist"))
                raise ValueError("User with such email address already exist")
            else:
                logger.error(f"Registration: " + str(err))
                raise

    @staticmethod
    def get_user_with_profile(username: str) -> dict:
        """Returns a dictionary of user fields joined with its profile
        Args:
            username (str): Login of a user
        Returns:
            dict: User fields plus associated profile fields
        """
        extended_user = User.query \
            .filter_by(login=username) \
            .join(Profile, User.id == Profile.user_id) \
            .with_entities(User.id, User.login, User.register_date,
                           Profile.name, Profile.email, Profile.birth_date) \
            .first()
        return dict(zip(extended_user._fields, extended_user._data))

    @staticmethod
    def verify_user(username: str, password: str) -> bool:
        """Verify provided username and password
        Args:
            username (str): Login
            password (str): Password
        Raises:
            ValueError: In case provided username not exist
        Returns:
            bool: If password was correct or not
        """
        user = User.query \
            .filter_by(login=username) \
            .with_entities(User.login, User.password_hash) \
            .first()
        if user is None:
            logger.error(f"Authentification: user with username: "
                         f"{username} does not exist")
            raise ValueError("User does not exist")
        elif not bcrypt.checkpw(password.encode("utf-8"),
                                user.password_hash.encode('utf-8')):
            logger.error(f"Authentification: password was incorrect "
                         f"for username: {user.login}")
            return False
        logger.info(f"Successfuly authentificated {username}")
        return True

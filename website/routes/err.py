from flask import render_template, request
from website import app, logger
from .main import main_menu
from ..services import log_msg


@app.errorhandler(404)
def not_found(error):
    """404 page"""
    logger.warning(log_msg(request, str(error)))
    return render_template("index.html",
                           title="Unable to find page you are looking for",
                           main_menu=main_menu)


@app.errorhandler(405)
def method_not_allowed(error):
    """405 page"""
    logger.warning(log_msg(request, str(error)))
    return render_template(
                    "index.html",
                    title="Such HTTP method is not allowed for this page",
                    main_menu=main_menu)

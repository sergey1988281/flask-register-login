from flask import flash, redirect, render_template, url_for, request, session
from typing import Callable
from functools import wraps
from os import path
from website import app, logger
from ..services import log_msg, PictureService, ProfileService, UserService
from ..forms import (LoginForm, RegisterForm, ChangePictureForm,
                     RotatePictureForm, SavePictureForm)


# Main menu items
main_menu = {
    "Main": "/",
    "Sign Up": "/signup",
    "Sign In": "/signin",
    "Log Off": "/logoff"
    }


def ensure_not_authentificated(route: Callable):
    """Decorator which sends all authentificated users to profile page"""
    @wraps(route)
    def decorated_route(*args, **kwargs):
        if "logged_user" in session:
            logger.info(
                log_msg(request,
                        f"Request authorised for {session['logged_user']}"))
            return redirect(
                        url_for("profile", username=session["logged_user"]))
        return route(*args, **kwargs)
    return decorated_route


def ensure_authentificated(route: Callable):
    """Decorator which checks if user authentificated otherwise
    sends to login page"""
    @wraps(route)
    def decorated_route(*args, **kwargs):
        if "logged_user" not in session:
            logger.warning(log_msg(request,
                                   (f"Requested login required page "
                                    f"but no active session exist")))
            return redirect(url_for("sign_in_form"))
        return route(*args, **kwargs)
    return decorated_route


def ensure_same_user(route: Callable):
    """Decorator which checks if user open its own profile
    otherwise sends to appropriate page"""
    @wraps(route)
    def decorated_route(*args, **kwargs):
        username = kwargs["username"]
        if username != session["logged_user"]:
            logger.warning(log_msg(
                request,
                (f"Requested profile of {username} but "
                 f"redirected to {session['logged_user']}")))
            return redirect(url_for("profile",
                                    username=session["logged_user"]))
        return route(*args, **kwargs)
    return decorated_route


@app.get(main_menu["Main"])
@ensure_not_authentificated
def index():
    """Main page route prints README"""
    with open('./README', 'r') as file:
        body = file.read().replace('\n', '<br>')
    logger.info(log_msg(request, "Main page opened"))
    return render_template(
            "index.html",
            title="Welcome to demo site!",
            content="<h2>Please complete registration and login</h2>" + body,
            main_menu=main_menu)


@app.get(main_menu["Sign Up"])
@ensure_not_authentificated
def sign_up_form():
    """Sign up page route which renders a form"""
    form = RegisterForm()
    logger.info(log_msg(request, "Registration page opened"))
    return render_template("signup.html",
                           title="Registration page",
                           main_menu=main_menu,
                           form=form)


@app.post(main_menu["Sign Up"])
@ensure_not_authentificated
def sign_up_logic():
    """Sign up page route which process submitted form"""
    form = RegisterForm()
    if form.validate_on_submit():
        logger.info(log_msg(request,
                            (f"Registration: attempt to register new user"
                             f"with username: {form.username.data}")))
        try:
            UserService.register_user(form.username.data,
                                      form.password1.data,
                                      form.email.data,
                                      form.birthdate.data)
        except (ValueError, Exception) as err:
            if isinstance(err, ValueError):
                flash("".join(err.args))
            else:
                flash("Unexpected error")
            return render_template("signup.html",
                                   title="Registration page",
                                   main_menu=main_menu,
                                   form=form)
        flash("Registration successful")
        return redirect(url_for("sign_in_form"))
    return render_template("signup.html",
                           title="Registration page",
                           main_menu=main_menu,
                           form=form)


@app.get(main_menu["Sign In"])
@ensure_not_authentificated
def sign_in_form():
    """Sign in page route which renders a form"""
    logger.info(log_msg(request, "Login page opened"))
    form = LoginForm()
    return render_template("signin.html",
                           title="Login page",
                           main_menu=main_menu,
                           form=form)


@app.post(main_menu["Sign In"])
@ensure_not_authentificated
def sign_in_logic():
    """Sign in page route which process submitted form"""
    form = LoginForm()
    if form.validate_on_submit():
        try:
            user_check = UserService.verify_user(form.username.data,
                                                 form.password.data)
            if not user_check:
                flash("Incorrect password")
                logger.warning(log_msg(request,
                                       (f"Authentification: Incorrect password"
                                        f"provided for {form.username.data}")))
            else:
                session["logged_user"] = form.username.data
                logger.info(log_msg(
                    request,
                    f"User {form.username.data} authentificated"))
                return redirect(url_for("profile",
                                        username=session["logged_user"]))
        except ValueError as err:
            flash("User does not exist")
            logger.error(log_msg(request,
                                 (f"Authentification: user with username: "
                                  f"{form.username.data} does not exist")))
    return render_template("signin.html",
                           title="Login page",
                           main_menu=main_menu,
                           form=form)


@app.get(main_menu["Log Off"])
@ensure_authentificated
def log_off():
    """Log off page route"""
    if "logged_user" in session:
        username = session.pop("logged_user")
        logger.info(log_msg(request, f"User {username} logged off"))
        title = f"User {username} logged off"
    else:
        logger.warning(log_msg(
                        request,
                        "Requested to logoff but no active session exist"))
        title = "Requested to logoff but no active session exist"
    return render_template("logoff.html",
                           title=title,
                           main_menu=main_menu)


@app.get("/profile/<username>")
@ensure_authentificated
@ensure_same_user
def profile(username):
    """Profile page route"""
    extended_user = UserService.get_user_with_profile(username)
    picture = ProfileService.ensure_picture_locally(
        username,
        extended_user["id"]
    )
    form = ChangePictureForm()
    form_rotate = RotatePictureForm()
    form_save = SavePictureForm()
    logger.info(log_msg(request, f"Displayed {username}`s profile"))
    return render_template("profile.html",
                           title=f"Profile of user {username}",
                           profiledata=extended_user,
                           picture=picture,
                           main_menu=main_menu,
                           form=form,
                           form_rotate=form_rotate,
                           form_save=form_save)


@app.post("/upload")
@ensure_authentificated
def upload_picture():
    """Uploading picture to server and saving to database logic"""
    form = ChangePictureForm()
    if form.validate_on_submit():
        try:
            final_filepath = ProfileService.save_picture_locally(
                username=session['logged_user'],
                file=form.picture.data
            )
        except Exception as err:
            flash("Unexpected error has occured on saving picture locally")
            logger.error(log_msg(
                            request,
                            (f"User {session['logged_user']} unexpected error"
                             f" on saving picture locally {str(err)}")))
        if final_filepath == "":
            flash('No file was specified')
            logger.warn(log_msg(request,
                                f"User {session['logged_user']} "
                                f"picture wasnt specified"))
            return redirect(url_for("profile",
                            username=session["logged_user"]))
        try:
            ProfileService.upload_picture(session["logged_user"])
            flash('Picture uploaded successful')
            logger.info(log_msg(
                            request,
                            f"Picture successfull uploaded {final_filepath}"))
        except Exception as err:
            flash("Unexpected error has occured on uploading picture to DB")
            logger.error(log_msg(
                            request,
                            (f"User {session['logged_user']} unexpected error"
                             f" on loading picture into database {str(err)}")))
    else:
        for field in form:
            if field.name not in ['csrf_token', 'submit']:
                [flash(e) for e in field.errors if field.errors]
    return redirect(url_for("profile", username=session["logged_user"]))


@app.post("/save")
@ensure_authentificated
def save_picture():
    """Saving picture logic"""
    form = SavePictureForm()
    if form.validate_on_submit():
        try:
            ProfileService.upload_picture(session["logged_user"])
            flash('Picture uploaded successful')
            logger.info(log_msg(
                            request,
                            (f"Picture successfull uploaded for user"
                             f"{session['logged_user']}")))
        except Exception as err:
            flash("Unexpected error with uploading picture to DB")
            logger.error(log_msg(
                            request,
                            (f"User {session['logged_user']} unexpected error"
                             f" on loading picture into database {str(err)}")))
    return redirect(url_for("profile", username=session["logged_user"]))


@app.post("/rotate")
@ensure_authentificated
def rotate_picture():
    """Rotate picture 90`"""
    form = RotatePictureForm()
    if form.validate_on_submit():
        try:
            PictureService.rotate_picture_90(
                ProfileService.get_picture_filepath(session["logged_user"])
            )
            flash('Picture rotated successfuly, do not forget to save')
            logger.info(log_msg(request,
                                (f"Picture successfull rotated for user "
                                 f"{session['logged_user']}")))
        except Exception as err:
            flash("Unexpected error with rotating picture")
            logger.error(log_msg(
                            request,
                            (f"User {session['logged_user']} unexpected error"
                             f" on rotating picture {str(err)}")))
    return redirect(url_for("profile", username=session["logged_user"]))

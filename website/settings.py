from os import environ
import logging


# Secret key is used for session encryption
SECRET_KEY = '192b9bdd22ab9ed4d12e236c78afcb9a393ec15f71bbf5dc987d54727823bcbf'
# SQL details are supposed to be in enviromental variables
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_DATABASE_URI = (f"postgresql://"
                           f"{environ['POSTGRES_USER']}"
                           f":"
                           f"{environ['POSTGRES_PASSWORD']}"
                           f"@"
                           f"{environ['POSTGRES_NAME']}"
                           f":5432/"
                           f"{environ['POSTGRES_DB']}")
# Debugging settings
# !!!Never leave enabled in Production!!!
SQLALCHEMY_ECHO = False
DEBUG_TB_PROFILER_ENABLED = False
DEBUG_TB_INTERCEPT_REDIRECTS = False
# Logger settings
LOGGING_LEVEL = logging.INFO
LOGGING_FORMAT = "%(levelname)s:%(asctime)s:%(message)s"
LOGGING_TARGET_FILE = "registerlogin.log"
# Picture settings
ALLOWED_PICTURE_EXTENSIONS = ('png', 'jpg', 'jpeg', 'gif')
MAX_PICTURE_SIZE = (200, 200)
STATIC_FOLDER = "website/static/"  # Do not change unless you certain
PICTURE_FOLDER = "img/"
UPLOAD_FOLDER = "tmp/"
# User policy
USERNAME_POLICY = {
    "restriction": r"^[A-Za-z][A-Za-z0-9-_]*[A-Za-z0-9]$",
    "restriction_notice": ("Username must start with letter, contain "
                           "only letters, digits, dashes or underscores")
}
PASSWORD_POLICY = {
    "min_len": 8,
    "min_len_notice": "Password must contain minimum 8 characters",
    "restriction": r"^[A-Za-z0-9-_\$%]*$",
    "restriction_notice": ("Password must not contain special"
                           " characters except % $ - _")
}

from website import db
from datetime import datetime


class User(db.Model):
    """This class defines table to store userid, login and password"""
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(50), unique=True, nullable=False)
    password_hash = db.Column(db.String(500), unique=False, nullable=False)
    register_date = db.Column(db.DateTime, default=datetime.utcnow)

from website import db


class Profile(db.Model):
    """This class defines table to store user profiles"""
    __tablename__ = "profiles"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=False, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    birth_date = db.Column(db.DateTime, unique=False, nullable=False)
    user_id = db.Column(db.Integer,
                        db.ForeignKey('users.id', ondelete="CASCADE"))
    picture = db.Column(db.LargeBinary, unique=False, nullable=True)

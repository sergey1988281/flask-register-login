import unittest
import website.unittests

testSuite = unittest.TestSuite()
testSuite.addTest(unittest.makeSuite(website.unittests.ValidateUser))
testSuite.addTest(unittest.makeSuite(website.unittests.ValidateProfile))

runner = unittest.TextTestRunner(verbosity=2)
runner.run(testSuite)

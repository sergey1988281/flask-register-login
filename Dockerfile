FROM python:3

WORKDIR /app

COPY . .

RUN pip3 install -r requirements.txt

EXPOSE 5000

ENV FLASK_ENV=Development

CMD ["gunicorn", "-b", "0.0.0.0:5000", "run:app"]